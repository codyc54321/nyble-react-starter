// core built in libraries like fs, etc

//third party
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';

// then your stuff
import Nav from './Nav';
import Home from './components/Home';
import Login from './components/Login';
import MapShowRestaurants from './components/MapShowRestaurants';


const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
    (
      <Provider store={createStoreWithMiddleware(reducers)}>
          <Router history={browserHistory}>
              <Route path="/" component={Nav}>
                  <IndexRoute component={Home}/>
                  <Route path='/map-show-restaurants' component={MapShowRestaurants}/>
              </Route>
          </Router>
      </Provider>
  ), document.querySelector('.container'));
