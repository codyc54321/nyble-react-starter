import React, { Component } from 'react';

import InputBox from './InputBox';


export default class BasicSignupInputs extends Component {
    render() {
        return (
            <div>
                <InputBox ref="email" form_id="email-input" type="email" label="Email:"/>
                <InputBox form_id="password-input" type="password" label="Password:"/>
            </div>
        )
    }
}
