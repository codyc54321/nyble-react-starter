import React, { Component } from 'react';


export default class InputBox extends Component {
    // from what I remember, the for (htmlFor in React) always points to the ID of that input...so these match
    // http://stackoverflow.com/questions/11008850/inputs-for-attribute-not-redirecting-to-input-box
    render() {
        return (
            <div className="form-group">
              <label htmlFor={this.props.form_id}>{this.props.label}</label>
              <input className="form-control" id={this.props.form_id} type={this.props.type}/>
            </div>
        )
    }
}
