import React, { Component } from 'react';

import InputBox from './InputBox';
import TextArea from './TextArea';


export default class BasicInfoInputs extends Component {
    render() {
        return (
            <div>
                <InputBox form_id="firstName-input" type="text" label="First Name:"/>
                <InputBox form_id="lastName-input" type="text" label="Last Name:"/>
                <InputBox form_id="phone-input" type="number" label="Phone Number:"/>
            </div>
        )
    }
}
