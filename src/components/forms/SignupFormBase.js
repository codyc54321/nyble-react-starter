import React, { Component } from 'react';
import { Link } from 'react-router';

import BasicSignupInputs from './BasicSignupInputs';


export default class SignupFormBase extends Component {
    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    render() {
      return (
        <div>
          <div className="jumbotron">
            <h1 className="text-center">Sign Up</h1>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">{this.props.header}</h3>
                </div>
                <div className="panel-body">
                  <form role="form">
                    <BasicSignupInputs />
                    {this.props.extraComponents}
                    <button className="btn btn-default" id="add-user" type="submit" onClick={this.submitForm}>Submit</button>
                    <Link activeClassName="active" to={this.props.actionTo} onlyActiveOnIndex>Submit</Link>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    submitForm(event) {
        console.log(firebase);
        event.preventDefault();
        alert(this.refs.email);
        firebase.auth().createUserWithEmailAndPassword("fake@example.com", "password234").catch(
            function(error) {
                alert(error)
                alert('firebase auth had an error');
            }
        );

    }
}
