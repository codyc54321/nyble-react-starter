import React, { Component } from 'react';
import { Link } from 'react-router';


export default class Login extends Component {
  render() {
    return (
      <div>
        <p>I would recommend using Firebase auth and keep all your data in Firebase:</p>
        <p><a href="https://firebase.google.com/docs/auth/" target="_blank">Intro docs</a>&nbsp;&nbsp;&nbsp;<a href="https://www.youtube.com/watch?v=-OKrloDzGpU" target="_blank">Intro Video</a></p>
        <label>Email:</label>
        <input type="text" />
        <label>Password:</label>
        <input type="password" />
        <Link activeClassName="active" to="/map-show-restaurants" onlyActiveOnIndex>Submit</Link>
      </div>
    );
  }
}
