import React, { Component } from 'react';
import { Link } from 'react-router';

import SignupFormBase from './forms/SignupFormBase';
import BasicInfoInputs from './forms/BasicInfoInputs';
import TextArea from './forms/TextArea';

// SignupFormGenerator
export default class SignupFormGenerator extends Component {

    constructor(props) {
        super(props);
        this.textAreaGenerator = this.textAreaGenerator.bind(this);
    }

    render() {
      let extraComponents = this.textAreaGenerator(this.props.form_data.textareas || []);

      return (
        <div>
            <SignupFormBase
              header={this.props.form_data.header}
              actionTo={this.props.form_data.actionTo}
              extraComponents={extraComponents} />
        </div>
      );
    }

    textAreaGenerator(textareas_data) {
        let textareas = textareas_data.map((data, index) => {
            return <TextArea form_id={data.form_id} rows={data.rows} label={data.label} key={index} />
        });
        return (
            <div>{textareas}</div>
        )
    }
}
