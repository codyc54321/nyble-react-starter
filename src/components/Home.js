import React, { Component } from 'react';

import Login from './Login';
import SignupFormGenerator from './SignupFormGenerator';

export default class Home extends Component {

    constructor(props) {
        super(props);

        let signup_form_data = {
            header: "New User Sign-Up",
            actionTo: "/map-show-restaurants",
        };

        let signup_form = (<SignupFormGenerator form_data={signup_form_data} />);

        this.state = {
            signup_form: signup_form
        }
    }

    render() {
        return (
            <div>
                <h3>Login</h3>
                <Login/>
                <h3>Sign Up</h3>
                {this.state.signup_form}
            </div>
        );
    }
}
