Online food ordering

Based on ReduxSimpleStarter <https://github.com/StephenGrider/ReduxSimpleStarter>

```
> git clone https://bitbucket.org/codyc54321/nyble-react-starter
> cd nyble-react-starter
> npm install
> npm start
> open your browser to http://127.0.0.1:8080
```

127.0.0.1 means "this machine". So for you, it means your laptop. On my laptop, it means my laptop to me, so it points to "right
here"

<https://www.howtogeek.com/126304/why-is-the-localhost-ip-127.0.0.1/>

8080 is a port we chose to use for the node server that runs the app. IP addresses are like house addresses, ports are like the
different doors you can use to enter your house
